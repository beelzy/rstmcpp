#pragma once
void DSPCorrelateCoefs(const short* source, int samples, short* coefsOut);
void DSPEncodeFrame(short pcmInOut[16], int sampleCount, unsigned char adpcmOut[8], const short coefsIn[8][2]);
