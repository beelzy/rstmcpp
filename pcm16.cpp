#include <stdexcept>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include "pcm16.h"
#include "endian.h"

using std::memcpy;
using namespace rstmcpp::pcm16;
using namespace rstmcpp::endian;

void PCM16::initWav(int channels, int sampleRate, int16_t* sample_data, int sample_count, int loop_start, int loop_end, bool keepMono) {
	if (channels > 2) throw std::invalid_argument("Streams of more than 2 channels not supported");
	if (channels <= 0) throw std::invalid_argument("Number of channels must be a positive integer");
	if (sampleRate <= 0) throw std::invalid_argument("Sample rate must be a positive integer");

	if (loop_start >= 0 && loop_end > (sample_count / channels)) {
		throw std::invalid_argument("The end of the loop is past the end of the file. Double-check the program that generated this data.");
	}

	this->channels = keepMono ? channels : 2;
	this->sampleRate = sampleRate;

	const int total_samples = (sample_count / channels) * this->channels;
	this->samples_backing = std::make_unique<int16_t[]>(total_samples);
	this->samples = this->samples_backing.get();
	this->samples_pos = this->samples;
	this->samples_end = this->samples + total_samples;

	if(channels == 2 || keepMono)
	{
		std::copy_n(sample_data, sample_count, this->samples);
	}
	else
	{
		for(int i = 0, j = 0; j < sample_count; i += 2, ++j)
		{
			this->samples[i] = sample_data[j];
			this->samples[i + 1] = sample_data[j];
		}
	}

	if (loop_start < 0) {
		this->looping = false;
		this->loop_start = this->samples;
		this->loop_end = this->samples_end;
	} else {
		this->looping = true;
		this->loop_start = this->samples + this->channels * loop_start;
		this->loop_end = this->samples + this->channels * loop_end;
	}
}

PCM16::PCM16(int channels, int sampleRate, int16_t* sample_data, int sample_count, bool keepMono) {
	initWav(channels, sampleRate, sample_data, sample_count, -1, -1, keepMono);
};

PCM16::PCM16(int channels, int sampleRate, int16_t* sample_data, int sample_count, int loop_start, int loop_end, bool keepMono) {
	initWav(channels, sampleRate, sample_data, sample_count, loop_start, loop_end, keepMono);
};

int PCM16::readSamples(void* destAddr, int numSamplesEachChannel) {
	int numSamplesTotal = numSamplesEachChannel * this->channels;

	if (this->samples_pos + numSamplesTotal > this->samples_end) {
		numSamplesTotal = this->samples_end - this->samples_pos;
	}

	memcpy(destAddr, this->samples_pos, sizeof(uint16_t) * numSamplesTotal);
	this->samples_pos += numSamplesTotal;

	return numSamplesTotal / this->channels;
}
